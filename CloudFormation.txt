+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CloudFormation top links:
    top:
        https://aws.amazon.com/cloudformation/
    User Guide:
        https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html
    How to use mappings in templates:
        https://medium.com/@emmasuzuki/aws-cloudformation-tip-modularization-6c43d23786fe
    How to import outputs from another stack:
        https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/walkthrough-crossstackref.html
    Intrinsic functions:
        https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference.html
    Exporting outputs:
        https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-stack-exports.html

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

YAML Tutorial:
    https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started/
YAML to JSON:
    https://www.json2yaml.com/


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CloudFormation quicklabs:
    Introduction to AWS CloudFormation
        https://www.qwiklabs.com/focuses/16447?catalog_rank=%7B%22rank%22%3A2%2C%22num_filters%22%3A1%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=9319083

    Creating an Amazon Virtual Private Cloud (VPC) with AWS CloudFormation
        https://www.qwiklabs.com/focuses/15515?catalog_rank=%7B%22rank%22%3A5%2C%22num_filters%22%3A1%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=9319102

    Introduction to AWS CloudFormation Designer
        https://www.qwiklabs.com/focuses/14149?catalog_rank=%7B%22rank%22%3A2%2C%22num_filters%22%3A1%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=9319138

    Challenge !!!
    Walkthrough: Refer to resource outputs in another AWS CloudFormation stack
        https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/walkthrough-crossstackref.html